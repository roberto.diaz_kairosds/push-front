import React from "react";

const App = () => (
    <div>
        <h1>POC push</h1>
        <button type="button" onClick={subscribe}>Suscribirse</button>
        <button type="button" >Notificar</button>
    </div>
);

const subscribe = () => {
    Notification.requestPermission()
    .then(permissions => {
        console.log(permissions);
        register(permissions)
    });

/*
    fetch('http://localhost:8080/subscriptions', {
        method: 'POST',
        body: JSON.stringify({
            id: crypto.randomUUID(),
            url: "url de prueba"
        }),
        headers: {
            'Content-Type': 'application/json; charset=UTF-8',
        },
    }).then(() => console.log('suscrito'));
*/
}

const register = (permissions) => {
    navigator.serviceWorker.getRegistration()
    .then(registration => {
        console.log(registration);
        const publicKey = getPublicKey();
        pushSubscribe(publicKey, registration);
    });
}

const getPublicKey = async () => {
    const response = await fetch('http://localhost:8080/public-key');
    return await response.text();
}

const pushSubscribe = (publicKey, registration) => {
    publicKey.then(data => {
        registration?.pushManager.subscribe({
            userVisibleOnly: true,
            applicationServerKey: urlB64ToUint8Array(data),
        })
        .then(subscription => console.log(subscription))
    });
}

function urlB64ToUint8Array(base64String) {
    const padding = '='.repeat((4 - (base64String.length % 4)) % 4);
    const base64 = (base64String + padding).replace(/\-/g, '+').replace(/_/g, '/');
    const rawData = window.atob(base64);
    const outputArray = new Uint8Array(rawData.length);
    for (let i = 0; i < rawData.length; ++i) {
      outputArray[i] = rawData.charCodeAt(i);
    }
    return outputArray;
}

export default App;